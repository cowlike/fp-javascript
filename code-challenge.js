var R = require('ramda');

//puzzle from: https://codefights.com/challenge/JGNL3YRDqC7By3fbu/main

var leftLens = R.lensIndex(0);
var rightLens = R.lensIndex(1);
var left = R.view(leftLens);
var right = R.view(rightLens);

//letter :: String -> Pair(Maybe String, Maybe String)
function letter(str) {
  var s = R.head(str)
  var nums = {
    2:['a','b','c'],
    3:['d','e','f'],
    4:['g','h','i'],
    5:['j','k','l'],
    6:['m','n','o'],
    7:['p','q','r','s'],
    8:['t','u','v'],
    9:['w','x','y','z']
  }
  var l = R.has(s, nums) ? nums[s][R.dec(R.length(str))] : undefined
  return l === undefined ? R.pair(str, undefined) : R.pair(undefined, l)
}

//f :: Pair([String], [String]) -> Pair(Maybe String, Maybe String) -> Pair([String], [String])
function f(acc, v) {
  var add = left(v) === undefined ?
    R.over(rightLens, R.append(right(v))) :
    R.over(leftLens, R.append(left(v)));

  return add(acc);
}

//reverse_t9 :: String -> Pair([String], String)
var reverse_t9 = R.pipe(R.groupWith(R.equals)
                      , R.filter(R.test(/[^ ]+/))
                      , R.map(letter)
                      , R.reduce(f, R.pair([], []))
                      , R.over(rightLens, R.join('')))

var log = console.log
log(reverse_t9('444   5555xxx     3')) //id
log(reverse_t9("44  44488    00143")) //hiugd
log(reverse_t9("44335557")) //help
log(reverse_t9("??foo44u333"))
