// loaded from
// http://tfs.ual.com:8080/tfs/defaultcollection/ADM.CommercialPortfolio/_apis/wit/workitemtypes/Bug?api-version=2.0

{
    "name": "Bug",
    "description": "Describes a divergence between required and actual behavior, and tracks the work done to correct the defect and verify the correction.",
    "xmlForm": "<FORM><Layout><Group><Column PercentWidth=\"100\"><Control FieldName=\"System.Title\" Type=\"FieldControl\" Label=\"&amp;Title:\" LabelPosition=\"Left\" /></Column></Group><Group><Column PercentWidth=\"50\"><Group Label=\"Status\"><Column PercentWidth=\"100\"><Control FieldName=\"System.AssignedTo\" Type=\"FieldControl\" Label=\"Assi&amp;gned To:\" LabelPosition=\"Left\" /><Control FieldName=\"System.State\" Type=\"FieldControl\" Label=\"&amp;State:\" LabelPosition=\"Left\" /><Control FieldName=\"System.Reason\" Type=\"FieldControl\" Label=\"&amp;Reason:\" LabelPosition=\"Left\" /><Control FieldName=\"Microsoft.VSTS.Common.ResolvedReason\" Type=\"FieldControl\" Label=\"Resolved Reason:\" LabelPosition=\"Left\" ReadOnly=\"True\" /></Column></Group></Column><Column PercentWidth=\"50\"><Group Label=\"Classification\"><Column PercentWidth=\"100\"><Control FieldName=\"System.AreaPath\" Type=\"WorkItemClassificationControl\" Label=\"&amp;Area:\" LabelPosition=\"Left\" /><Control FieldName=\"System.IterationPath\" Type=\"WorkItemClassificationControl\" Label=\"Ite&amp;ration:\" LabelPosition=\"Left\" /></Column></Group></Column></Group><Group><Column PercentWidth=\"100\"><Group Label=\"Planning\"><Column PercentWidth=\"33\"><Control FieldName=\"Microsoft.VSTS.Common.StackRank\" Type=\"FieldControl\" Label=\"Stack Ran&amp;k:\" LabelPosition=\"Left\" NumberFormat=\"DecimalNumbers\" MaxLength=\"10\" /></Column><Column PercentWidth=\"33\"><Control FieldName=\"Microsoft.VSTS.Common.Priority\" Type=\"FieldControl\" Label=\"&amp;Priority:\" LabelPosition=\"Left\" /></Column><Column PercentWidth=\"33\"><Control FieldName=\"Microsoft.VSTS.Common.Severity\" Type=\"FieldControl\" Label=\"Se&amp;verity:\" LabelPosition=\"Left\" /></Column></Group></Column></Group><TabGroup><Tab Label=\"Details\"><Group><Column PercentWidth=\"50\"><Control FieldName=\"Microsoft.VSTS.TCM.ReproSteps\" Type=\"HtmlFieldControl\" Label=\"Steps to Repro&amp;duce:\" LabelPosition=\"Top\" Dock=\"Fill\" MinimumSize=\"(100,200)\" /></Column><Column PercentWidth=\"50\"><Control FieldName=\"System.History\" Type=\"WorkItemLogControl\" Label=\"&amp;History:\" LabelPosition=\"Top\" Dock=\"Fill\" /></Column></Group></Tab><Tab Label=\"System Info\"><Group><Column PercentWidth=\"50\"><Control FieldName=\"Microsoft.VSTS.Build.FoundIn\" Type=\"FieldControl\" Label=\"&amp;Found in Build:\" LabelPosition=\"Left\" /></Column><Column PercentWidth=\"50\"><Control FieldName=\"Microsoft.VSTS.Build.IntegrationBuild\" Type=\"FieldControl\" Label=\"In&amp;tegrated in Build:\" LabelPosition=\"Left\" /></Column></Group><Control FieldName=\"Microsoft.VSTS.TCM.SystemInfo\" Type=\"HtmlFieldControl\" Label=\"System I&amp;nfo:\" LabelPosition=\"Top\" Dock=\"Fill\" /></Tab><Tab Label=\"Test Cases\"><Control Type=\"LinksControl\" Label=\"Test &amp;Cases testing this Bug:\" LabelPosition=\"Top\" Name=\"TestedBy\"><LinksControlOptions><LinkColumns><LinkColumn RefName=\"System.ID\" /><LinkColumn RefName=\"System.WorkItemType\" /><LinkColumn RefName=\"System.Title\" /><LinkColumn RefName=\"System.AssignedTo\" /><LinkColumn RefName=\"System.State\" /><LinkColumn LinkAttribute=\"System.Links.Comment\" /></LinkColumns><WorkItemLinkFilters FilterType=\"include\"><Filter LinkType=\"Microsoft.VSTS.Common.TestedBy\" FilterOn=\"forwardname\" /></WorkItemLinkFilters><ExternalLinkFilters FilterType=\"excludeAll\" /><WorkItemTypeFilters FilterType=\"include\"><Filter WorkItemType=\"Test Case\" /></WorkItemTypeFilters></LinksControlOptions></Control></Tab><Tab Label=\"All Links\"><Control Type=\"LinksControl\" LabelPosition=\"Top\" Name=\"GeneralLinks\"><LinksControlOptions><LinkColumns><LinkColumn RefName=\"System.ID\" /><LinkColumn RefName=\"System.WorkItemType\" /><LinkColumn RefName=\"System.Title\" /><LinkColumn RefName=\"System.AssignedTo\" /><LinkColumn RefName=\"System.State\" /><LinkColumn LinkAttribute=\"System.Links.Comment\" /></LinkColumns></LinksControlOptions></Control></Tab><Tab Label=\"Attachments\"><Control Type=\"AttachmentsControl\" LabelPosition=\"Top\" /></Tab></TabGroup></Layout></FORM>",
    "fieldInstances": [{
        "helpText": "The iteration within which this bug will be fixed",
        "referenceName": "System.IterationPath",
        "name": "Iteration Path",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.IterationPath"
    }, {
        "referenceName": "System.IterationId",
        "name": "IterationID",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.IterationId"
    }, {
        "referenceName": "System.ExternalLinkCount",
        "name": "ExternalLinkCount",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.ExternalLinkCount"
    }, {
        "referenceName": "System.TeamProject",
        "name": "Team Project",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.TeamProject"
    }, {
        "referenceName": "System.HyperLinkCount",
        "name": "HyperLinkCount",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.HyperLinkCount"
    }, {
        "referenceName": "System.AttachedFileCount",
        "name": "AttachedFileCount",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AttachedFileCount"
    }, {
        "referenceName": "System.NodeName",
        "name": "Node Name",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.NodeName"
    }, {
        "helpText": "The area of the product with which this bug is associated",
        "referenceName": "System.AreaPath",
        "name": "Area Path",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AreaPath"
    }, {
        "referenceName": "System.RevisedDate",
        "name": "Revised Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.RevisedDate"
    }, {
        "referenceName": "System.ChangedDate",
        "name": "Changed Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.ChangedDate"
    }, {
        "referenceName": "System.Id",
        "name": "ID",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Id"
    }, {
        "referenceName": "System.AreaId",
        "name": "AreaID",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AreaId"
    }, {
        "referenceName": "System.AuthorizedAs",
        "name": "Authorized As",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AuthorizedAs"
    }, {
        "helpText": "Stories affected and how",
        "referenceName": "System.Title",
        "name": "Title",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Title"
    }, {
        "helpText": "Change on reviewing, fixing or verifying the fix. Active = not yet fixed; Resolved = fix not yet verified; Closed = fix verified.",
        "referenceName": "System.State",
        "name": "State",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.State"
    }, {
        "referenceName": "System.AuthorizedDate",
        "name": "Authorized Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AuthorizedDate"
    }, {
        "referenceName": "System.Watermark",
        "name": "Watermark",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Watermark"
    }, {
        "referenceName": "System.Rev",
        "name": "Rev",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Rev"
    }, {
        "referenceName": "System.ChangedBy",
        "name": "Changed By",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.ChangedBy"
    }, {
        "helpText": "The reason why the bug is in the current state",
        "referenceName": "System.Reason",
        "name": "Reason",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Reason"
    }, {
        "helpText": "The person currently working on this bug",
        "referenceName": "System.AssignedTo",
        "name": "Assigned To",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.AssignedTo"
    }, {
        "referenceName": "System.WorkItemType",
        "name": "Work Item Type",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.WorkItemType"
    }, {
        "referenceName": "System.CreatedDate",
        "name": "Created Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.CreatedDate"
    }, {
        "referenceName": "System.CreatedBy",
        "name": "Created By",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.CreatedBy"
    }, {
        "referenceName": "System.Description",
        "name": "Description",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Description"
    }, {
        "helpText": "Discussion thread plus automatic record of changes",
        "referenceName": "System.History",
        "name": "History",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.History"
    }, {
        "referenceName": "System.BISLinks",
        "name": "BIS Links",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.BISLinks"
    }, {
        "referenceName": "System.RelatedLinkCount",
        "name": "RelatedLinkCount",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.RelatedLinkCount"
    }, {
        "referenceName": "System.Tags",
        "name": "Tags",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.Tags"
    }, {
        "referenceName": "System.BoardColumn",
        "name": "Board Column",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.BoardColumn"
    }, {
        "referenceName": "System.BoardColumnDone",
        "name": "Board Column Done",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.BoardColumnDone"
    }, {
        "referenceName": "System.BoardLane",
        "name": "Board Lane",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/System.BoardLane"
    }, {
        "helpText": "Work first on items with lower-valued stack rank. Set in triage.",
        "referenceName": "Microsoft.VSTS.Common.StackRank",
        "name": "Stack Rank",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.StackRank"
    }, {
        "helpText": "Assessment of the effect of the bug on the project",
        "referenceName": "Microsoft.VSTS.Common.Severity",
        "name": "Severity",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.Severity"
    }, {
        "helpText": "Business importance. 1=must fix; 4=unimportant.",
        "referenceName": "Microsoft.VSTS.Common.Priority",
        "name": "Priority",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.Priority"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ClosedBy",
        "name": "Closed By",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ClosedBy"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ClosedDate",
        "name": "Closed Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ClosedDate"
    }, {
        "helpText": "The reason why the bug was resolved",
        "referenceName": "Microsoft.VSTS.Common.ResolvedReason",
        "name": "Resolved Reason",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ResolvedReason"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ResolvedBy",
        "name": "Resolved By",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ResolvedBy"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ResolvedDate",
        "name": "Resolved Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ResolvedDate"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ActivatedBy",
        "name": "Activated By",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ActivatedBy"
    }, {
        "referenceName": "Microsoft.VSTS.Common.ActivatedDate",
        "name": "Activated Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.ActivatedDate"
    }, {
        "referenceName": "Microsoft.VSTS.Common.StateChangeDate",
        "name": "State Change Date",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Common.StateChangeDate"
    }, {
        "helpText": "The build in which the bug was fixed",
        "referenceName": "Microsoft.VSTS.Build.IntegrationBuild",
        "name": "Integration Build",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Build.IntegrationBuild"
    }, {
        "helpText": "How to see the bug. End by contrasting expected with actual behavior.",
        "referenceName": "Microsoft.VSTS.TCM.ReproSteps",
        "name": "Repro Steps",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.TCM.ReproSteps"
    }, {
        "helpText": "Test context, provided automatically by test infrastructure",
        "referenceName": "Microsoft.VSTS.TCM.SystemInfo",
        "name": "System Info",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.TCM.SystemInfo"
    }, {
        "helpText": "The build in which the bug was found",
        "referenceName": "Microsoft.VSTS.Build.FoundIn",
        "name": "Found In",
        "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/_apis/wit/fields/Microsoft.VSTS.Build.FoundIn"
    }],
    "transitions": {
        "": [{
            "to": "Active"
        }],
        "Active": [{
            "to": "Active"
        }, {
            "to": "Resolved",
            "actions": ["Microsoft.VSTS.Actions.Checkin"]
        }],
        "Resolved": [{
            "to": "Resolved"
        }, {
            "to": "Active"
        }, {
            "to": "Closed"
        }],
        "Closed": [{
            "to": "Closed"
        }, {
            "to": "Active"
        }]
    },
    "_links": {
        "self": {
            "href": "http://tfs.ual.com:8080/tfs/DefaultCollection/b6d37e2d-d850-4053-bd94-0fac5565a9e2/_apis/wit/workItemTypes/Bug"
        }
    },
    "url": "http://tfs.ual.com:8080/tfs/DefaultCollection/b6d37e2d-d850-4053-bd94-0fac5565a9e2/_apis/wit/workItemTypes/Bug"
}
