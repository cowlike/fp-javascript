//Some simple predicates

var R = require('ramda');

function smallNum(x) { return x < 100; }
function pos(x) { return x >= 0; }
function even(x) { return x % 2 === 0; }
function odd(x) { return !even(x); }

//manually write filter

function filter(pred, arr) {
  var result = [];
  arr.forEach(
    function (x) {
      if (pred(x)) result.push(x);
    });
  return result;
}

//Compose a couple of the predicates

var smallOdd = R.compose(R.filter(smallNum), R.filter(odd));

//Need to curry my manual version of filter to compose it

var filter2 = R.curry(filter);
var evenPos = R.compose(filter2(pos), filter2(even));

// Try mapping over an Object

var foo = R.mapObjIndexed(
  function(v,k,o) {return typeof(v) === "number" ? v * 100 : v;});

//----------------------------------

var zipEq = R.zipWith(function (x,y) {return x == y;});
var same = R.compose(R.all(R.identity), zipEq);

/*
** isPalindrome("otto"); //true
** isPalindrome([1,2,3,2,1]); //true
** isPalindrome("no"); //false
*/
function isPalindrome(xs) {
  return same(xs, R.reverse(xs));
}

//----------------------------------
// return a Fibonacci generator

function fibStream() {
  var n = 0;
  var n0 = 1;
  var n1 = 1;
  return function() {
    if (n >= 2) {
      var t = n0;
      n0 = n1;
      n1 = t + n0;
    }
    n++;
    return n1;
  };
}

var fibs = R.times(fibStream()); // fibs(30);

//----------------------------------
var first = R.head;
var second = R.compose(R.head, R.drop(1));

function iter(fStep, init) {
  var next = init;
  return function () {
    next = R.append(fStep(next), next);
    return next;
  };
}

//----------------------------------
function msgAfterTimeout (msg, who, timeout, onDone) {
    setTimeout(function () {
        onDone(msg + " Hello " + who + "!");
    }, timeout);
}

console.log(smallOdd([1,2,3,4]));

console.log(Math.sqrt(5))
console.log(Math.pow(10,7))

function tri(n) {
  var prev = 1;
  for (x = prev; x <= Math.sqrt(n); x++) {
    cur = x * (x+1) / 2;
    if (cur*cur + prev*prev == n) {
      return true;
    }
    prev = cur;
  }
  return false;
}

console.log(tri(6));
console.log(tri(45));
