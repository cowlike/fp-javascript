// Mastermind from this Haskell exercise:
// http://www.seas.upenn.edu/~cis194/hw/02-lists.pdf

var R = require('ramda');

var l = console.log;

var RED = 0;
var GREEN = 1;
var BLUE = 2;
var YELLOW = 3;
var ORANGE = 4;
var PURPLE = 5;
var colors = [RED, GREEN, BLUE, YELLOW, ORANGE, PURPLE];

// exactMatches0 :: [Color] -> [Color] -> Int
function exactMatches0(secret, guess) {
  var lst = R.zipWith(R.equals, secret, guess);
  return R.reduce(function(acc,v) {return v ? R.inc(acc) : acc}, 0, lst);
}
// exact0 :: [Color] -> [Color] -> Int
var exact0 = R.compose(
  R.reduce(function(acc,v) {return v ? R.inc(acc) : acc}, 0),
  R.zipWith(R.equals)
);

// exactMatches1 :: [Color] -> [Color] -> Int
function exactMatches1(secret, guess) {
  var lst = R.zipWith(R.equals, secret, guess);
  return R.length(R.filter(R.identity, lst));
}
// exact1 :: [Color] -> [Color] -> Int
var exact1 = R.compose(
  R.length,
  R.filter(R.identity),
  R.zipWith(R.equals)
);

// exactMatches2 :: [Color] -> [Color] -> Int
function exactMatches2(secret, guess) {
  var lst = R.zipWith(R.equals, secret, guess);
  return R.sum(R.map(function(v) {return v ? 1 : 0}, lst));
}
// exact2 :: [Color] -> [Color] -> Int
var exact2 = R.compose(
  R.sum,
  R.map(function(v) {return v ? 1 : 0}),
  R.zipWith(R.equals)
);

var secret = [RED,GREEN,BLUE,YELLOW];
var guess = [RED,PURPLE,GREEN,YELLOW];
// l(exactMatches0(secret, guess));
// l(exactMatches1(secret, guess));
// l(exactMatches2(secret, guess));
// l(exact0(secret, guess));
// l(exact1(secret, guess));
// l(exact2(secret, guess));

//var colors = [RED, GREEN, BLUE, YELLOW, ORANGE, PURPLE];
// countColors :: Code -> [Int]
function countColors(code) {
  var result = [0,0,0,0,0,0];
  for (var i = 0; i < 4; i++) {
    result[code[i]] += 1;
  }
  return result;
}

function countColors1(code) {
  return R.reduce(
    function(acc,v) {return R.update(v, ++acc[v], acc)},
    [0,0,0,0,0,0],
    code);
}

var countColors2 = R.reduce(
  function(acc,v) {return R.update(v, ++acc[v], acc)},
  [0,0,0,0,0,0]);

// l(countColors([RED,GREEN,GREEN,PURPLE]))
// l(countColors1([RED,GREEN,GREEN,PURPLE]))
// l(countColors2([RED,GREEN,GREEN,PURPLE]))
// l(R.equals(countColors([GREEN, BLUE, GREEN, ORANGE]),[0, 2, 1, 0, 1, 0]))

//matches :: Code -> Code -> Int
function matches(secret, guess) {
  return R.sum(R.zipWith(R.min, countColors1(secret), countColors1(guess)));
}

var matches1 = R.curry(function (secret, guess) {
  return R.sum(R.zipWith(R.min, countColors1(secret), countColors1(guess)));
});

// l(matches (
//   [RED, BLUE, ORANGE, YELLOW, ORANGE],
//   [RED, ORANGE, ORANGE, ORANGE, BLUE]))

//getMove :: Code -> Code -> Move
//Move :: {code::Code, exact::Int, nonexact::Int}
function getMove(secret, guess) {
  var total = matches(secret, guess);
  var ex = exact2(secret, guess);
  return {code:guess,
    exact:ex,
    nonexact:total - ex};
}

// l(getMove([RED, BLUE, YELLOW, ORANGE], [RED, ORANGE, ORANGE, BLUE]))

//isConsistent :: Move -> Code -> Bool
var isConsistent = R.curry(function (move, secret) {
  var exact = exact2(secret, move.code);
  var non = matches(secret, move.code) - exact;
  return exact == move.exact && non == move.nonexact;
});

// l(isConsistent({code:[RED,RED,BLUE,GREEN],exact:1,nonexact:1}, [RED,BLUE,YELLOW,PURPLE]));
// l(isConsistent({code:[RED,RED,BLUE,GREEN],exact:1,nonexact:1}, [RED,BLUE,RED,PURPLE]));

//filterCodes :: Move -> [Code] -> [Code]
var filterCodes = R.curry(function (move, codes) {
  return R.filter(isConsistent(move), codes)
});

function iterate(f, arg, n) {
  if (n === 1) {
    return f(arg);
  }
  else {
    return iterate(f, f(arg), n-1);
  }
}
// allCodes :: Int -> [Code]
function allCodes(length) {
  return iterate(codeHelper, [[]], length);
}

//var colors = [RED, GREEN, BLUE, YELLOW, ORANGE, PURPLE];
var codeHelper = R.curry(
  function (codes) {
    return R.chain(growCode, codes);
  });

var appendColor = R.curry(
  function (code,color) {
    return R.append(color,code);
  });

var growCode = R.curry(
  function (code) {
    return R.map(appendColor(code), colors)
  });

function solveHelper(secret, guess, remaining, moves) {
  var move = getMove(secret, guess);
  var moves = R.append(move, moves);

  if (R.equals(secret, guess)) {
    return moves;
  }
  else {
    var remaining = filterCodes(move, remaining);
    return solveHelper(secret,
      R.head(remaining),
      R.tail(remaining),
      moves);
  }
}

//solve :: Code -> [Move]
function solve(secret) {
  var len = R.length(secret);
  var guess = R.repeat(RED, len);
  return solveHelper(secret, guess, allCodes(len), []);
}

l(solve([BLUE,BLUE,BLUE,BLUE,BLUE]));
