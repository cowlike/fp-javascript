var IO = {
  getJSON: R.curry(function(user, pw, url, callback) {
    var options = {
      method: "GET",
      cache: false,
      url: url,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      timeout: 5000,
      beforeSend: function(xhr) {
        xhr.setRequestHeader("authorization", "Basic " + btoa(user + ":" + pw));
      },
      success: callback,
      error: function(request, errorType, errorThrown) {
        alert(errorType + ', ' + errorThrown);
      }
    };
    $.ajax(options);
  }),

  drawGraph: R.curry(function(container, json) {
    var nodes = new vis.DataSet(nodeList(json.transitions));
    var edges = new vis.DataSet(edgeList(json.transitions));

    // create a network
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {
      physics: {
        enabled: true,
        barnesHut: {
          gravitationalConstant: -5000,
          centralGravity: 0.3,
          springLength: 95,
          springConstant: 0.04,
          damping: 0.09,
          avoidOverlap: 0
        }
      }
    };
    var network = new vis.Network(container, data, options);
  })
}

var getURL = R.curry(function(baseURL, collection, project, workItemName) {
  return baseURL +
    '/' + collection +
    '/' + project +
    '/_apis/wit/workItemTypes/' +
    workItemName +
    '?api-version=2.0';
})

var tfsURL = getURL('https://tfs.ual.com/tfsgit/');

//graph functions----------------------------------------
var transformNodeName = R.ifElse(R.equals(""), _ => "Init", R.identity);

function mkNode(name) {
  return {
    id: name,
    label: name,
    'shape': 'box'
  };
};

var nodeList = R.pipe(R.keys, R.map(R.compose(mkNode, transformNodeName)));

var mkEdge = R.curry(function(key, val) {
  return {
    from: transformNodeName(key),
    to: transformNodeName(val.to),
    arrows: {
      to: {
        scaleFactor: .25
      }
    }
  };
});

var edgeList = R.compose(R.flatten, R.values,
  R.mapObjIndexed((v, k, o) => R.map(mkEdge(k), R.filter(x => x.to != k, v))));
//-----------------------------------------------------

function app(location, user, pw, collection, project, wi) {
  var draw = IO.drawGraph(location);
  IO.getJSON(user, pw, tfsURL(collection, project, wi), draw);
}
