//Credit card validation from this Haskell exercise:
//http://www.seas.upenn.edu/~cis194/hw/01-intro.pdf
//sample message

var R = require('ramda');

var l = console.log;

// lastDigit :: Integer -> Integer
var lastDigit = R.compose(parseInt, R.last, R.toString);

// dropLastDigit :: Integer -> Integer
var dropLastDigit = R.compose(parseInt, R.init, R.toString);

// toRevDigits :: Integer -> [Integer]
var toRevDigits = R.ifElse(
  R.gte(0),
  R.always([]),
  R.compose(R.map(parseInt), R.reverse, R.toString)
)

// cycle :: [a] -> Integer -> [a]
var cycle = R.compose(R.flatten, R.repeat);

// doubleEveryOther :: [Integer] -> [Integer]
function doubleEveryOther(xs) {
  return R.zipWith(R.multiply, xs, cycle([1,2], xs.length));
}

// sumDigits :: [Integer] -> Integer
var sumDigits = R.compose(R.sum, R.chain(toRevDigits));

// luhn :: Integer -> Bool
var luhn = R.compose(
    R.equals(0),
    R.flip(R.modulo)(10),
    sumDigits,
    doubleEveryOther,
    toRevDigits);

//very simple test harness
var tests = [
  R.equals(3, lastDigit(123))
  ,R.equals(12, dropLastDigit(123))
  ,R.equals([4, 3,2,1], toRevDigits(1234))
  ,R.equals([], toRevDigits(-17))
  ,R.equals([5,5,9,4], toRevDigits(4955))
  ,R.equals([1,2,3,1,2,3,1,2,3], cycle([1,2,3], 3))
  ,R.equals([1,2,1,2,1,2,1,2], cycle([1,2], 4))
  ,R.equals(['a','b','c','a','b','c','a','b','c'], cycle(['a','b','c'], 3))
  ,R.equals([4,18,5,10], doubleEveryOther([4,9,5,5]))
  ,R.equals([4,18,5], doubleEveryOther([4,9,5]))
  ,R.equals([0,0], doubleEveryOther([0,0]))
  ,R.equals(19, sumDigits([10, 5, 18, 4]))
  ,luhn(5594589764218858)
  ,R.not(luhn(1234567898765432))
  ,luhn(4222222222222)
]

var test = R.ifElse(
  R.identity,
  R.T,
  function(v, i) { l('test ' + i + ' failed!'); return false; }
)
var iMap = R.addIndex(R.map)
l('all tests: ' + R.all(R.identity, iMap(test, tests)))
